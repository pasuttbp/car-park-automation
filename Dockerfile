FROM node:10.19.0-alpine

ARG ENV_APP
ENV envValue=$ENV_APP

RUN apk add --no-cache tzdata
RUN ln -snf /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && echo Asia/Bangkok > /etc/timezone

COPY . /app
WORKDIR /app
RUN npm install --only=prod
EXPOSE 9080
CMD ["sh", "-c", "npm run $envValue"]