const { Router } = require('express');
const router = Router(); // Load router
const { logger } = require('../../utils/logger');
const CONFIG = require('../../config');
const { response, responseError, genResponseObj } = require('../../errors');
const { mockData } = require('../../mock-data/mock-data-table')

logger.root.info('Loading server api routes');
//  Load routes for out controllers
logger.root.info('Server api routes loaded');

router.use('/mock', async (req, res) => {
	try {
		await mockData();
		return response(res, genResponseObj(req.get('x-language'), '20000', 'mock up data success', undefined, CONFIG.NODE));
	} catch (e) {
		return response(res, genResponseObj(req.get('x-language'), '50000', e, undefined, CONFIG.NODE));
	}
});

router.use(async (req, res, next) => {
	if (req.get('x-language')) {
		if (![ 'th', 'en' ].includes(req.get('x-language'))) {
			return responseError(res, genResponseObj(req.get('x-language'), '40001', 'x-language is not match', undefined, CONFIG.NODE));
		}
	}
	if (!req.get('x-app-name')) {
		return responseError(res, genResponseObj(req.get('x-language'), '40100', 'x-app-name is empty', undefined, CONFIG.NODE));
	} else if (!req.get('x-channel')) {
		return responseError(res, genResponseObj(req.get('x-language'), '40100', 'x-channel is empty', undefined, CONFIG.NODE));
	} else if (!req.get('x-transaction-id')) {
		return responseError(res, genResponseObj(req.get('x-language'), '40100', 'x-transaction-id is empty', undefined, CONFIG.NODE));
	} else if (!req.get('x-auth-role')) {
		return responseError(res, genResponseObj(req.get('x-language'), '40100', 'x-auth-role is empty', undefined, CONFIG.NODE));
	} else if (req.get('x-auth-role') !== CONFIG.ROLE.GUEST && req.get('x-auth-role') !== CONFIG.ROLE.STAFF) {
		return responseError(res, genResponseObj(req.get('x-language'), '40100', 'x-auth-role is not correct value', undefined, CONFIG.NODE));
	} else {
		logger.root.info('validate header success');
		next();
	}
});

router.use('/parking', require('./parking.routes'));

module.exports = router;
