const { Router } = require('express');
const router = Router(); // Load router
const { logger } = require('../../utils/logger');
const controller = require('../../controllers/parking.controller');
const { parkingValidators, validation, property } = require('../../validators')
logger.root.info('parking.routes loaded');

router.get('/status', validation(parkingValidators.carSizeSchema, property.QUERY), controller.getStatusParkingLot);

router.get('/slot', validation(parkingValidators.carSizeSchema, property.QUERY),
	controller.getRegistrationAllocatedSlotNumberListByCarSize);

router.post('/', validation(parkingValidators.carSchema, property.BODY), controller.createParkingLot);

router.get('/plate-number', validation(parkingValidators.carSizeSchema, property.QUERY),
	controller.getRegistrationPlateNumberListByCarSize);

router.patch('/park', validation(parkingValidators.carNumberPlateSchema, property.BODY),
	controller.parkTheCar);

router.patch('/leave', validation(parkingValidators.carNumberPlateSchema, property.BODY),
	controller.leaveTheSlot);

module.exports = router;
