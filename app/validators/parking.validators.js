const Joi = require('@hapi/joi');
const { CAR } = require('../config/constants')

const carSchema = Joi.object({
	carNumberPlate: Joi.string().required().messages({
		'any.required': 'carNumberPlate is required',
	}),
	carSize: Joi.string().valid(CAR.SIZE.S, CAR.SIZE.M, CAR.SIZE.L).required().messages({ 'any.required': 'carSize is required' }),
	serial: Joi.string().optional().messages({
		'any.required': 'serial is required',
	}),
});

const carSizeSchema = Joi.object({
	carSize: Joi.string().valid(CAR.SIZE.S, CAR.SIZE.M, CAR.SIZE.L).required().messages({ 'any.required': 'carSize is required' }),
});

const carNumberPlateSchema = Joi.object({
	carNumberPlate: Joi.string().required().messages({
		'any.required': 'carNumberPlate is required',
	}),
});

module.exports = { carSchema, carSizeSchema, carNumberPlateSchema };
