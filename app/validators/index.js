const { responseError, genResponseObj } = require('../errors');
const { NODE } = require('../config');
const { logger } = require('../utils/logger');
const parkingValidators = require('./parking.validators');

const property = {
	BODY: 'body',
	QUERY: 'query',
	PARAMS: 'params',
};

const validation = (schema, prop = property.BODY) => {
	return (req, res, next) => {
		const { error, value } = schema.validate(req[prop]);
		if (error) {
			if (error.details[0].type === 'any.required') {
				return responseError(res, genResponseObj(req.get('x-language'), '40001', error.details[0].message, undefined, NODE));
			} else {
				return responseError(res, genResponseObj(req.get('x-language'), '40000', error.details[0].message, undefined, NODE));
			}
		} else {
			logger.root.info(value, req);
			next();
		}
	};
};

const validateSchema = (req, schema, data) => {
	const { error, value } = schema.validate(data);
	if (error) {
		if (error.details[0].type === 'any.required') {
			throw genResponseObj(req.get('x-language'), '40001', error.details[0].message, undefined, NODE);
		} else {
			throw genResponseObj(req.get('x-language'), '40000', error.details[0].message, undefined, NODE);
		}
	} else {
		return value;
	}
};

module.exports = {
	validation,
	validateSchema,
	property,
	parkingValidators
};
