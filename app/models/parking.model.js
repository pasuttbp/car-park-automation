const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
	class Parking extends Model {
	}

	Parking.init({
		parkingId: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
			field: 'PARKING_ID'
		},
		status: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'STATUS',
		},
		roleId:{
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'ROLE_ID',
		},
		carSize: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'CAR_SIZE',
		},
		floor: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'FLOOR',
		},
		xAxis: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'X_AXIS',
		},
		yAxis: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Y_AXIS',
		},
	}, {
		sequelize,
		modelName: 'Parking',
		tableName: 'PARKING',
		updatedAt: 'UPDATED_DATE',
		createdAt: 'CREATED_DATE'
	});

	Parking.associations = (models) => {
		Parking.belongsTo(models['Role'], {
			foreignKey: 'roleId',
			constraints: false,
		});
		Parking.hasMany(models['TransactionHistory'], {
			foreignKey: 'parkingId',
			constraints: false,
		});

	};

	return Parking;
};

