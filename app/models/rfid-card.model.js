const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class RfidCard extends Model {
  }

  RfidCard.init(
    {
      rfidCardId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'RFID_CARD_ID',
      },
      serial: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        field: 'SERIAL',
      },
      roleId:{
        type: DataTypes.INTEGER,
        allowNull: false,
        field: 'ROLE_ID',
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'STATUS',
      }
    },
    { sequelize, modelName: 'RfidCard', tableName: 'RFID_CARD', updatedAt: 'UPDATED_DATE', createdAt: 'CREATED_DATE' }
  );

  RfidCard.associations = (models) => {
    RfidCard.belongsTo(models['Role'], {
      foreignKey: 'roleId',
      constraints: false,
    });
    RfidCard.belongsTo(models['Staff'], {
      foreignKey: 'rfidCardId',
      constraints: false,
    });
    RfidCard.hasMany(models['TransactionHistory'], {
      foreignKey: 'rfidCardId',
      constraints: false,
    });
  };
  return RfidCard;
};
