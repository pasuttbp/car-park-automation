const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Role extends Model {
  }

  Role.init(
    {
      roleId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'ROLE_ID',
      },
      roleCode: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        field: 'ROLE_CODE',
      },
      roleName: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        field: 'ROLE_NAME',
      },
    },
    { sequelize, modelName: 'Role', tableName: 'ROLE', updatedAt: 'UPDATED_DATE', createdAt: 'CREATED_DATE' }
  );

  Role.associations = (models) => {
    Role.hasMany(models['Staff'], {
      foreignKey: 'roleId',
      constraints: false
    });
    Role.hasMany(models['TransactionHistory'], {
      foreignKey: 'roleId',
      constraints: false
    });
    Role.hasMany(models['Parking'], {
      foreignKey: 'roleId',
      constraints: false,
    });
    Role.hasMany(models['RfidCard'], {
      foreignKey: 'roleId',
      constraints: false,
    });
  };
  return Role;
};
