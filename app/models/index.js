const { postgreSql } = require('../db-connect');
const { Op } = require('sequelize');
const { MOCK_UP } = require('../config')
let models = {
	TransactionHistory: postgreSql.import('./transaction-history.model.js'),
	Parking: postgreSql.import('./parking.model.js'),
	RfidCard: postgreSql.import('./rfid-card.model.js'),
	Staff: postgreSql.import('./staff.model.js'),
	Role: postgreSql.import('./role.model.js'),
};

Object.keys(models).forEach((name) => {
	if (typeof models[name].associations === 'function') {
		models[name].associations(models);
	}
});

if (MOCK_UP === 'N') {
	postgreSql.sync({
		force: true
	});
} else {
	postgreSql.sync()
}

module.exports = { models, Op };
