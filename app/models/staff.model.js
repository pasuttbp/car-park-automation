const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Staff extends Model {
  }

  Staff.init(
    {
      staffId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'STAFF_ID',
      },
      staffCode: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        field: 'STAFF_CODE',
      },
      staffName: { type: DataTypes.STRING, allowNull: false, field: 'STAFF_NAME' },
      staffPhone: { type: DataTypes.STRING, field: 'STAFF_PHONE' },
      staffEmail: { type: DataTypes.STRING, field: 'STAFF_EMAIL' },
      staffCarSize: { type: DataTypes.STRING, field: 'STAFF_CAR_SIZE' },
      staffCarNumberPlate: { type: DataTypes.STRING, field: 'STAFF_CAR_NUMBER_PLATE' },
      rfidCardId: { type: DataTypes.INTEGER, field: 'RFID_CARD_ID' },
      roleId: { type: DataTypes.INTEGER, field: 'ROLE_ID' },
    },
    { sequelize, modelName: 'Staff', tableName: 'STAFF', updatedAt: 'UPDATED_DATE', createdAt: 'CREATED_DATE' }
  );

  Staff.associations = (models) => {
    Staff.hasMany(models['RfidCard'], {
      foreignKey: 'rfidCardId',
      constraints: false,
    });
    Staff.hasMany(models['TransactionHistory'], {
      foreignKey: 'staffId',
      constraints: false,
    });
    Staff.belongsTo(models['Role'], {
      foreignKey: 'roleId',
      constraints: false,
    });
  };
  return Staff;
};
