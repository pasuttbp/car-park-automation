const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
	class TransactionHistory extends Model {
	}

	TransactionHistory.init({
		transactionHistoryId: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
			field: 'TRANSACTION_HISTORY_ID'
		},
		serial: {
			type: DataTypes.STRING,
			field: 'SERIAL',
			allowNull: false,
		},
		staffId: {
			type: DataTypes.INTEGER,
			field: 'STAFF_ID',
		},
		parkingId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'PARKING_ID',
		},
		carNumberPlate: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'CAR_NUMBER_PLATE',
		},
		carSize: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'CAR_NUMBER_SIZE',
		},
		roleId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'ROLE_ID',
		},
		rfidCardId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'RFID_CARD_ID',
		},
		loginTime: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('now'),
			field: 'LOGIN_TIME',
		},
		logoutTime: {
			type: DataTypes.DATE,
			field: 'LOGOUT_TIME',
		},
		isRevoke: {
			type: DataTypes.BOOLEAN,
			field: 'IS_REVOKE',
			allowNull: false,
			defaultValue: false
		},
	}, {
		sequelize,
		modelName: 'TransactionHistory',
		tableName: 'TRANSACTION_HISTORY',
		timestamps: false
	});

	TransactionHistory.associations = (models) => {
		TransactionHistory.belongsTo(models['RfidCard'], {
			foreignKey: 'rfidCardId',
			constraints: false,
		});
		TransactionHistory.belongsTo(models['Staff'], {
			foreignKey: 'staffId',
			constraints: false,
		});
		TransactionHistory.belongsTo(models['Parking'], {
			foreignKey: 'parkingId',
			constraints: false,
		});
		TransactionHistory.belongsTo(models['Role'], {
			foreignKey: 'roleId',
			constraints: false,
		});
	};

	return TransactionHistory;
};

