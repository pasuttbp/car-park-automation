const { Op } = require('sequelize');
const { sqlServer } = require('../db-connect');
const { replaceSpecialCharacterInQuery } = require('../utils/db.util');

const findByPk = async (id, model) => {
	try {
		const res = await model.findByPk(id);
		return res ? res.dataValues : res;
	} catch (e) {
		throw e;
	}
};

const findOne = async (criteria, included, model, attributes = [], order) => {
	let options = { include: included, where: criteria };
	if (Array.isArray(attributes) && attributes.length) {
		options.attributes = attributes;
	}
	if (order !== undefined) {
		options.order = order
	}
	try {
		const res = await model.findOne(options);
		return res ? res.dataValues : res;
	} catch (e) {
		throw e;
	}
};

const findAll = async (criteria, included, model, attributes = [], options = null, groupBy = null) => {
	let searchOptions = { include: included, where: criteria };
	if (options) {
		const { order, limit, offset, search } = options;
		if (order) {
			searchOptions.order = order;
		}
		if (limit) {
			searchOptions.limit = limit;
		}
		if (offset) {
			searchOptions.offset = offset;
		}
		if (search) {
			let { fields, value } = search;
			value = value ? value.trim() : '';
			const searchList = fields.map((field) => ({
				[field]: {
					[Op.like]: replaceSpecialCharacterInQuery(`${ value }`),
				},
			}));
			if (value) {
				searchOptions.where = {
					...criteria,
					[Op.or]: searchList,
				};
			}
		}
	}
	if (Array.isArray(attributes) && attributes.length) {
		searchOptions.attributes = attributes;
	}
	if (Array.isArray(groupBy) && groupBy.length) {
		searchOptions.group = groupBy;
	}
	try {
		return await model.findAll(searchOptions);
	} catch (e) {
		throw e;
	}
};

const findAndCountAll = async (criteria, included, model, attributes = [], options = null) => {
	let searchOptions = { include: included, where: criteria };
	if (options) {
		const { order, limit, offset, search } = options;
		if (order) {
			searchOptions.order = order;
		}
		if (limit) {
			searchOptions.limit = limit;
		}
		if (offset) {
			searchOptions.offset = offset;
		}
		if (search) {
			let { fields, value } = search;
			value = value ? value.trim() : '';
			const searchList = fields.map((field) => ({
				[field]: {
					[Op.like]: replaceSpecialCharacterInQuery(`${ value }`),
				},
			}));
			if (value) {
				searchOptions.where = {
					...criteria,
					[Op.or]: searchList,
				};
			}
		}
	}
	if (Array.isArray(attributes) && attributes.length) {
		searchOptions.attributes = attributes;
	}
	try {
		const res = await model.findAndCountAll(searchOptions);
		return {
			items: res.rows,
			totalItems: res.count
		};
	} catch (e) {
		throw e;
	}
};

const create = async (data, model, transaction) => {
	try {
		return await model.create(data, { transaction }).then((result) => {
			return result.get({ plain: true });
		});
	} catch (e) {
		throw e;
	}
};

const createWithAssociations = async (data, include, model, transaction) => {
	try {
		return await model.create(data, { include, transaction }).then((result) => {
			return result.get({ plain: true });
		}).catch(err => {
			throw err;
		});
	} catch (e) {
		throw e;
	}
};

const update = async (data, criteria, model, transaction) => {
	try {
		return await model.update(data, {
			where: criteria,
			transaction: transaction
		});
	} catch (e) {
		throw e;
	}
};

const query = async (sql, replacements = []) => {
	try {
		return await sqlServer.query(sql, { replacements, type: sqlServer.QueryTypes.SELECT }).then(row => {
			return row.length === 1 ? row[0] : row;
		});
	} catch (e) {
		throw e;
	}

};

const queryWithCriteria = async (sql, criteria = {}) => {
	try {
		return await sqlServer.query(sql, { replacements: criteria }, { type: sqlServer.QueryTypes.SELECT }).then(row => {
			let result;
			if (row.length === 1) {
				result = row[0];
			} else {
				result = row;
			}
			return result;
		});
	} catch (e) {
		throw e;
	}
};

const bulkCreate = async (data, model, transaction) => {
	try {
		return await model.bulkCreate(data, { transaction }).then((result) => {
			return result;
		});
	} catch (e) {
		throw e;
	}
};

const destroy = async (criteria, model, transaction) => {
	try {
		return await model.destroy({ where: criteria, transaction });
	} catch (e) {
		throw e;
	}
};

const upsert = async (data, criteria, model, include, transaction) => {
	try {
		return await model
			.findOne({ where: criteria })
			.then(async (obj) => {
				if (obj) {
					// update
					const updateData = await obj.update(data, { transaction });
					return updateData.get({ plain: true });
				} else {
					// insert
					const createData = await model.create(data, { include, transaction });
					return createData.get({ plain: true });
				}
			});
	} catch (e) {
		throw e;
	}
};

module.exports = {
	findByPk,
	findOne,
	findAndCountAll,
	create,
	update,
	query,
	queryWithCriteria,
	bulkCreate,
	findAll,
	createWithAssociations,
	destroy,
	upsert
};
