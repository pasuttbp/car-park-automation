const CONFIG = require('./config'); // Load config (environment)
const Sequelize = require('sequelize');
const { logger } = require('./utils/logger');

logger.root.info(`Starting: API PORT : ${ CONFIG.PORT }`);
logger.root.info(`Node ENV : ${ CONFIG.ENV }`);
logger.root.info(`Connecting DB : ${ JSON.stringify(CONFIG.DB) }`);

const postgreSql = new Sequelize(CONFIG.DB.POSTGRESQL.DB_NAME, CONFIG.DB.POSTGRESQL.USER, CONFIG.DB.POSTGRESQL.PASS, {
	host: CONFIG.DB.POSTGRESQL.ADDR,
	port: CONFIG.DB.POSTGRESQL.PORT,
	dialect: 'postgres',
	logging: false,
	dialectOptions: {
		options: {
			connectTimeout: 15000,
			requestTimeout: 20000,
			encrypt: true,
			enableArithAbort: true
		},
	},
	pool: {
		max: 100,
		min: 0,
		acquire: 60000,
		idle: 10000
	}
});

const connectPostgreSql = () => {
	return new Promise((resolve, reject) => {
		postgreSql.authenticate().then(() => {
			logger.root.info('PostgreSql connection has been established successfully.');
			resolve(postgreSql);
		}).catch((err) => {
			logger.root.error(`Unable to connect to the database PostgreSql: ${ typeof err === 'object' ? err.stack !== undefined ? err.stack : err : err }`);
			reject(err);
		});
	});
};

const establishConnection = () => {
	const i = 0;
	retryConnectPostgreSql(i);
};

const retryConnectPostgreSql = (i) => {
	connectPostgreSql().then(() => {
		logger.root.info('Connect PostgreSql success')
	}).catch(err => {
		logger.root.info(err);
		if (i < 2) {
			i++;
			retryConnectPostgreSql(i);
		}
	});
};

establishConnection();

process.on('uncaughtException', (err) => {
	logger.root.info(err);
	process.exit(1);
});

module.exports = { postgreSql };
