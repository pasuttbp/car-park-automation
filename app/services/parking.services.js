const { logger } = require('../utils/logger');
const { models } = require('../models');
const { findOne, findAndCountAll, create, update } = require('../db');
const { PARKING } = require('../config/constants');

exports.getTransactionHistoryByCriteria = async (req, criteria, included) => {
	logger.root.debug('parking.services get Transaction History by criteria', req);
	return await findOne(criteria, included, models['TransactionHistory']);
};

exports.addTransactionHistory = async (req, data, transaction) => {
	logger.root.debug('parking.services add Transaction History', req);
	return await create(data, models['TransactionHistory'], transaction);
};

exports.updateTransactionHistoryByCriteria = async (req, data, criteria, transaction) => {
	logger.root.debug('parking.services update transaction history by criteria', req);
	return await update(data, criteria, models['TransactionHistory'], transaction);
};

exports.updateRfidCardByCriteria = async (req, data, criteria, transaction) => {
	logger.root.debug('parking.services update rfid card by criteria', req);
	return await update(data, criteria, models['RfidCard'], transaction);
};

exports.updateParkingByCriteria = async (req, data, criteria, transaction) => {
	logger.root.debug('parking.services update parking by criteria', req);
	return await update(data, criteria, models['Parking'], transaction);
};

exports.getAllocatedSlotNumber = async (req, carSize) => {
	logger.root.debug('parking.services get allocated slot number', req);
	return await findOne({ carSize, status: PARKING.STATUS.INACTIVE }, [ {
		model: models['Role'],
		required: true,
		where: {
			roleCode: req.get('x-auth-role')
		},
	} ], models['Parking'], [], [ [ 'floor', 'ASC' ], [ 'yAxis', 'ASC' ], [ 'xAxis', 'ASC' ] ]);
};

exports.getRegistrationAllocatedSlotNumberListByCarSize = async (req) => {
	logger.root.debug('parking.services get registration allocated slot number list by car size', req);
	const { carSize } = req.query;
	return await findAndCountAll({ carSize, status: PARKING.STATUS.INACTIVE }, [ {
		model: models['Role'],
		required: true,
		where: {
			roleCode: req.get('x-auth-role')
		},
		attributes: []
	} ], models['Parking'], [
		'parkingId',
		'status',
		'carSize',
		'floor',
		'xAxis',
		'yAxis', ], [ [ 'floor', 'ASC' ], [ 'yAxis', 'ASC' ], [ 'xAxis', 'ASC' ] ]);
};

exports.getRfidCardByCriteria = async (req, criteria, included) => {
	logger.root.debug('parking.services get rfid card by criteria', req);
	return await findOne(criteria, included, models['RfidCard']);
};

exports.getStaffByCriteria = async (req, criteria, included) => {
	logger.root.debug('parking.services get staff by criteria', req);
	return await findOne(criteria, included, models['Staff']);
};

exports.getRegistrationPlateNumberListByCarSize = async (req) => {
	logger.root.debug('parking.services get registration plate number list by car size', req);
	const { carSize } = req.query;
	return await findAndCountAll({ carSize, isRevoke: false }, [ {
		model: models['Role'],
		required: true,
		where: {
			roleCode: req.get('x-auth-role')
		},
		attributes: []
	} ], models['TransactionHistory'], [ 'transactionHistoryId', 'serial', 'carNumberPlate', 'carSize', 'loginTime' ]);
};
