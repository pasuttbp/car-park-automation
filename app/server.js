const CONFIG = require('./config'); // Load config (environment)
const express = require('express'); // Load express
const bodyParser = require('body-parser'); // Load bodyParser
const cors = require('cors'); // Load cors
const hbs = require('hbs');
const helmet = require('helmet');
const db = require('./db');
const routes = require('./routes');
const https = require('https');
const constants = require('constants');
const fs = require('fs');
//  Create the app
// ============================================================================================
const app = express();
// const app = express(db);

app.set('view engine', 'hbs');

app.enable('trust proxy');

// configure app to use bodyParser()
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000,
}));

app.use(bodyParser.json({
    limit: '50mb',
}));


// Helmet
app.use(helmet());

app.use(helmet.contentSecurityPolicy({
    directives: {
        defaultSrc: ['\'self\''],
    }
}));

app.use(helmet.noCache());

// Enable CORS on Express server instance
if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'azdev') {
    app.use(
        cors({
            credentials: true,
            origin: true,
            exposedHeaders: ['x-auth'],
            methods: 'GET,POST,PUT,PATCH,DELETE',
        }),
    );
} else {
    app.use(
        cors({
            credentials: true,
            origin: CONFIG.CORS.ORIGIN,
            exposedHeaders: ['x-auth'],
            methods: 'GET,POST,PUT,PATCH,DELETE',
            optionsSuccessStatus: 405
        }),
    );
}

// Configure app routes
app.use('/cpa/v1', routes.v1);

// not found
app.use((req, res) => (res.status(404).send()));

// Start up the Node Express server
if (
  CONFIG.HTTPS.CERT_PATH &&
  CONFIG.HTTPS.KEY_PATH &&
  (CONFIG.ENV === 'iot' || CONFIG.ENV === 'production')
) {
  try {
    const options = {
      key: fs.readFileSync(CONFIG.HTTPS.KEY_PATH),
      cert: fs.readFileSync(CONFIG.HTTPS.CERT_PATH),
      secureOptions:
        constants.SSL_OP_NO_SSLv3 | constants.SSL_OP_NO_SSLv2 | constants.SSL_OP_NO_TLSv1 | constants.SSL_OP_NO_TLSv1_1,
      ciphers: [
        'ECDHE-RSA-AES256-SHA384',
        'DHE-RSA-AES256-SHA384',
        'ECDHE-RSA-AES256-SHA256',
        'DHE-RSA-AES256-SHA256',
        'ECDHE-RSA-AES128-SHA256',
        'DHE-RSA-AES128-SHA256',
        'HIGH',
        '!SHA1',
        '!aNULL',
        '!eNULL',
        '!EXPORT',
        '!DES',
        '!RC4',
        '!MD5',
        '!PSK',
        '!SRP',
        '!CAMELLIA',
      ].join(':'),
      honorCipherOrder: true,
    };
    https.createServer(options, app).listen(CONFIG.PORT, () => {
      // console.log('Start server with SSL');
      // console.log(`Node Express server listening on port ${CONFIG.PORT}`);
    });
  } catch (error) {
    // console.log('Start server without SSL');
    app.listen(CONFIG.PORT);
  }
} else {
  // console.log('Start server without SSL');
  app.listen(CONFIG.PORT);
}
