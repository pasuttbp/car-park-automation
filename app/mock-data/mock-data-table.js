const { models } = require('../models')
const { bulkCreate } = require('../db')
const { postgreSql } = require('../db-connect')
const { PARKING, ROLE, RFID, CAR } = require('../config/constants')
const CONFIG = require('../config')
const { generateSerial } = require('../utils')

exports.mockData = async () => {
	const transaction = await postgreSql.transaction();
	try {
		await bulkCreate(service.parking(), models['Parking'], transaction);
		await bulkCreate(service.role(), models['Role'], transaction);
		const rfidStaffList = await bulkCreate(service.rfidCard().staff, models['RfidCard'], transaction);
		await bulkCreate(service.rfidCard().guest, models['RfidCard'], transaction);
		await bulkCreate(service.staff(rfidStaffList), models['Staff'], transaction);
		await transaction.commit();
	} catch (e) {
		await transaction.rollback();
	}

}

const service = {
	role: () => {
		return [
			model.role('0', CONFIG.ROLE.STAFF, ROLE.STAFF),
			model.role('1', CONFIG.ROLE.GUEST, ROLE.GUEST)
		];
	},
	parking: () => {
		const data = []
		for (let f = 1; f <= 3; f++) {
			for (let x = 1; x <= 3; x++) {
				for (let y = 1; y <= 3; y++) {
					if (x === 1) {
						data.push(model.parking(PARKING.STATUS.INACTIVE, 0, functionCarSize(y), f, x, y))
					} else {
						data.push(model.parking(PARKING.STATUS.INACTIVE, 1, functionCarSize(y), f, x, y))
					}
				}
			}
		}
		return data;
	},
	rfidCard: () => {
		const data = {
			staff: [],
			guest: []
		}
		for (let i = 0; i < 9; i++) {
			data.staff.push(model.rfidCard(generateSerial(), 0, RFID.STATUS.ACTIVE))
		}
		for (let i = 0; i < 18; i++) {
			data.guest.push(model.rfidCard(generateSerial(), 1, RFID.STATUS.INACTIVE))
		}
		return data
	},
	staff: (rfidStaffList) => {
		const data = [];
		let carSize = 1;
		for (let i = 0; i < rfidStaffList.length; i++) {
			data.push(model.staff(`CPA${ generateSerial() }`,
				`staff_${ i }`, `080000000${ i }`,
				'pasut@tester.com',
				functionCarSize(carSize), `CAR000000${ i }`, rfidStaffList[i].rfidCardId,
				0))
			if (carSize === 3) {
				carSize = 1;
			} else {
				carSize = carSize + 1;
			}
		}
		return data;
	}

}

const functionCarSize = (y) => {
	let carSize;
	switch (y) {
		case 1:
			carSize = CAR.SIZE.S;
			break;
		case 2:
			carSize = CAR.SIZE.M;
			break;
		default:
			carSize = CAR.SIZE.L;
			break
	}
	return carSize
}

const model = {
	parking: (status, roleId, carSize, floor, xAxis, yAxis) => ({
		status,
		roleId,
		carSize,
		floor,
		xAxis,
		yAxis
	}),
	rfidCard: (serial, roleId, status) => ({
		serial,
		roleId,
		status
	}),
	role: (roleId, roleCode, roleName) => ({ roleId, roleCode, roleName }),
	staff: (staffCode, staffName, staffPhone, staffEmail, staffCarSize, staffCarNumberPlate, rfidCardId, roleId) => ({
		staffCode,
		staffName,
		staffPhone,
		staffEmail,
		staffCarSize,
		staffCarNumberPlate,
		rfidCardId,
		roleId,
	})

}



