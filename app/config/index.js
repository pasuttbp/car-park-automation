const path = require('path');

// import .env variables
if (!process.env.NODE_ENV) {
	require('dotenv-safe')
		.config({
			path: path.join(__dirname, './env/.env.dev'),
			sample: path.join(__dirname, './env/.env'),
		});
} else if (process.env.NODE_ENV === 'sqa') {
	require('dotenv-safe')
		.config({
			path: path.join(__dirname, './env/.env.sqa'),
			sample: path.join(__dirname, './env/.env'),
		});
} else if (process.env.NODE_ENV === 'iot') {
	require('dotenv-safe')
		.config({
			path: path.join(__dirname, './env/.env.iot'),
			sample: path.join(__dirname, './env/.env'),
		});
} else if (process.env.NODE_ENV === 'dev') {
	require('dotenv-safe')
		.config({
			path: path.join(__dirname, './env/.env.dev'),
			sample: path.join(__dirname, './env/.env'),
		});
} else if (process.env.NODE_ENV === 'production') {
	require('dotenv-safe')
		.config({
			path: path.join(__dirname, './env/.env.production'),
			sample: path.join(__dirname, './env/.env'),
		});
}

module.exports = {
	NODE: process.env.NODE_SERVER,
	CORS: {
		ORIGIN: process.env.CORS_ORIGIN
	},
	HTTPS: {
		CERT_PATH: process.env.HTTPS_CERT_PATH,
		KEY_PATH: process.env.HTTPS_KEY_PATH,
	},
	DB: {
		POSTGRESQL: {
			USER: process.env.POSTGRESQL_USER,
			PASS: process.env.POSTGRESQL_PASS,
			ADDR: process.env.POSTGRESQL_ADDR,
			PORT: process.env.POSTGRESQL_PORT,
			DB_NAME: process.env.POSTGRESQL_DB_NAME,
		},
	},
	ENV: process.env.NODE_ENV,
	PORT: process.env.PORT,
	LOG: {
		NAME: process.env.LOG_NAME,
		ROOT_PATH: process.env.LOG_ROOT_PATH,
		FILE_LEVEL: process.env.LOG_FILE_LEVEL, // error, warn, info, verbose, debug, silly
		CONSOLE_LEVEL: process.env.LOG_CONSOLE_LEVEL, // error, warn, info, verbose, debug, silly
	},
	ROLE: {
		STAFF: process.env.ROLE_STAFF,
		GUEST: process.env.ROLE_GUEST
	},
	MOCK_UP:  process.env.MOCK_UP // Y or N
};
