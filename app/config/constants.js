const { resMessage } = require('./message.properties');

module.exports = {

	resCode: {
		20000: resMessage.common.success,
		40000: resMessage.badRequest.invalidParameter,
		40001: resMessage.badRequest.parameterIsMissing,
		40002: resMessage.badRequest.duplicate,
		40100: resMessage.unauthorized.loginWithWrongUserOrPassword,
		40101: resMessage.unauthorized.anotherSessionStillLogon,
		40102: resMessage.unauthorized.userLockedTime,
		40300: resMessage.forbidden.sessionExpired,
		40301: resMessage.forbidden.forceUpdate,
		40302: resMessage.forbidden.sessionRevoked,
		40303: resMessage.forbidden.accessTokenInvalid,
		40304: resMessage.forbidden.roleNotMatchService,
		40400: resMessage.notFound.urlNotFound,
		40401: resMessage.notFound.dataNotFound,
		50000: resMessage.common.error,
		50001: resMessage.db.databaseError,
		50002: resMessage.db.connectionFail,
	},

	LOG_CONSTANT: {
		LEVEL: {
			FATAL: 'fatal',
			ERROR: 'error',
			WARN: 'warn',
			INFO: 'info',
			DEBUG: 'debug',
		},
		LOG_TYPE_SERVICE: {
			PARKING: 'parking',
		}
	},

	CAR: {
		SIZE: {
		S: 'S',
		M: 'M',
		L: 'L',
		}
	},
	PARKING: {
		STATUS: {
			ACTIVE: 'Active',
			INACTIVE: 'Inactive',
			PENDING_ACTIVE: 'Pending Active',
		}
	},

	RFID: {
		STATUS: {
			ACTIVE: 'Active',
			INACTIVE: 'Inactive',
		}
	},
	ROLE: {
		GUEST: 'GUEST',
		STAFF: 'STAFF'
	}
};
