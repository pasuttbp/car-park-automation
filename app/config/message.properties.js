module.exports = {
  resMessage: {
    common: {
      success: {
        en: 'Transaction success.',
        th: 'ทำรายการสำเร็จ',
      },
      notFound: {
        en: 'Data Not Found.',
        th: 'ไม่พบข้อมูล',
      },
      error: {
        en: 'Sorry, The system is not available at this time.',
        th: 'ขออภัย ระบบไม่สามารถให้บริการได้ในขณะนี้',
      },
      requireField: {
        en: 'Invalid Data',
        th: 'ข้อมูล Data ไม่ถูกต้อง',
      }
    },
    badRequest: {
      invalidParameter: {
        en: 'Invalid format of parameter',
        th: 'ข้อมูลที่กรอกไม่ถูกต้อง',
      },
      parameterIsMissing: {
        en: 'Missing parameter',
        th: 'ข้อมูลที่กรอกไม่ถูกต้อง',
      },
      duplicate: {
        en: 'Duplicate data',
        th: 'ข้อมูลซ้ำกับในระบบ',
      },
    },
    unauthorized: {
      loginWithWrongUserOrPassword: {
        en: 'Username or password incorrect, please try again',
        th: 'ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง',
      },
      anotherSessionStillLogon: {
        en: 'Cannot access because there is another session still logon',
        th: 'ท่านไม่สามารถเข้าใช้งานได้ เนื่องจากมีผู้ใช้งานอีกท่านในระบบ',
      },
      userLockedTime: {
        en: 'Your account has been locked. Please wait a moment or contact admin',
        th: 'บัญชีของคุณถูกล็อค กรุณารอสักครู่ หรือติดต่อผู้ดูแลระบบ',
      },
    },
    forbidden: {
      sessionExpired: {
        en: 'Session expired, please log in again',
        th: 'เปิดหน้าจอค้างนานเกินระยะเวลาที่กำหนด กรุณาเข้าสู่ระบบใหม่อีกครั้ง',
      },
      accessTokenInvalid: {
        en: 'Access token invalid, please log in again',
        th: 'ไม่สามารถหา token ของท่านได้ กรุณาเข้าสู่ระบบใหม่อีกครั้ง',
      },
      forceUpdate: {
        en: 'Please update the application before use',
        th: 'กรุณาอัพเดทแอปพลิเคชันก่อนการใช้งาน',
      },
      sessionRevoked: {
        en: 'Your session has been revoked. Please log in again',
        th: 'การใช้งานของท่านถูกยกเลิกแล้ว กรุณาเข้าสู่ระบบใหม่อีกครั้ง',
      },
      roleNotMatchService: {
        en: 'You do not have access to use. Please contact the system administrator and log in again.',
        th: 'ท่านไม่มีสิทธิ์เข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ และเข้าสู่ระบบใหม่อีกครั้ง',
      },
    },
    notFound: {
      urlNotFound: {
        en: 'Your URL path is incorrect. Please contact the system administrator',
        th: 'ไม่พบบริการที่คุณร้องขอ กรุณาติดต่อผู้ดูแลระบบ',
      },
      dataNotFound: {
        en: 'Data not found',
        th: 'ไม่พบข้อมูล',
      },
      customerOrderNoNotFound: {
        en: 'Customer Order No not found',
        th: 'ไม่พบข้อมูล Customer Order No',
      }
    },
    db: {
      connectionFail: {
        en: 'Failed to establish a connection to the database. Pleae try again later.',
        th: 'ไม่สามารถเชื่อมต่อฐานข้อมูลได้. กรุณาลองใหม่ภายหลัง',
      },
      databaseError: {
        en: 'Database error.',
        th: 'เกิดข้อผิดพลาดที่ฐานข้อมูล',
      },
    },
  },
};
