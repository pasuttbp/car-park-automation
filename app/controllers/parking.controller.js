const { logger } = require('../utils/logger');
const { genResponseObj, response, responseError } = require('../errors');
const service = require('../services/parking.services');
const { postgreSql } = require('../db-connect');
const { NODE, ROLE } = require('../config');
const { RFID, PARKING } = require('../config/constants');
const { models, Op } = require('../models');

exports.createParkingLot = async (req, res) => {
	logger.root.debug('parking.controller create parking lot', req);
	try {
		const transaction = await postgreSql.transaction();
		try {
			const { carNumberPlate, carSize, serial } = req.body;
			const parkingSlot = await service.getAllocatedSlotNumber(req, carSize);
			if (parkingSlot === null) {
				throw genResponseObj(req.get('x-language'), '40401', 'parking slot is full', undefined, NODE);
			}
			const data = {
				parkingId: parkingSlot.parkingId,
				roleId: parkingSlot.roleId,
				rfidCardId: null,
				staffId: null,
				serial,
				carNumberPlate,
				carSize
			};
			if (ROLE.STAFF === req.get('x-auth-role')) {
				const staff = await service.getStaffByCriteria(req, { roleId: parkingSlot.roleId, },
					[ { model: models['RfidCard'], where: { serial, }, required: true } ]);
				if (staff === null) {
					throw genResponseObj(req.get('x-language'), '40401', 'serial not found', undefined, NODE);
				}
				if (staff.staffCarNumberPlate !== carNumberPlate || staff.staffCarSize !== carSize) {
					throw genResponseObj(req.get('x-language'), '40000', 'car number plate or car size invalid ', undefined, NODE);
				}
				data.staffId = staff.staffId;
				data.rfidCardId = staff.rfidCardId;
			} else {
				const rfid = await service.getRfidCardByCriteria(req, {
					roleId: parkingSlot.roleId,
					status: RFID.STATUS.INACTIVE
				});
				data.rfidCardId = rfid.rfidCardId;
				data.serial = rfid.serial;
			}
			const checkDuplicate = await service.getTransactionHistoryByCriteria(req,
				{
					[Op.or]: [
						{ serial: data.serial },
						{ carNumberPlate }
					]
				}
			);
			if (checkDuplicate !== null) {
				if (checkDuplicate.isRevoke === false) {
					throw genResponseObj(req.get('x-language'), '40003', 'Serial duplicate, Please contact the system administrator', undefined, NODE);
				}
			}
			const transactionHistory = await service.addTransactionHistory(req, data, transaction);
			await service.updateParkingByCriteria(req, { status: PARKING.STATUS.PENDING_ACTIVE }, { parkingId: transactionHistory.parkingId }, transaction);
			if (ROLE.GUEST === req.get('x-auth-role')) {
				await service.updateRfidCardByCriteria(req, { status: RFID.STATUS.ACTIVE }, { rfidCardId: transactionHistory.rfidCardId }, transaction);
			}
			await transaction.commit();
			return response(res, genResponseObj(req.get('x-language'), '20000', 'create parking lot success', '', NODE));
		} catch (e) {
			await transaction.rollback();
			if (e && e.resultCode) {
				throw e;
			} else {
				logger.root.error(e, req);
				throw genResponseObj(req.get('x-language'), '50000', e.message, '', NODE);
			}
		}
	} catch (e) {
		if (e && e.resultCode) {
			return responseError(res, e);
		} else {
			return responseError(res, genResponseObj(req.get('x-language'), '50000', e.message, '', NODE));
		}
	}
};

exports.getStatusParkingLot = async (req, res) => {
	logger.root.debug('parking.controller get status parking lot', req);
	try {
		const { carSize } = req.query;
		const result = await service.getAllocatedSlotNumber(req, carSize);
		return response(res, genResponseObj(req.get('x-language'), '20000', 'get status parking lot success', { statusParkingLot: result !== null }, NODE));
	} catch (e) {
		if (e && e.resultCode) {
			return responseError(res, e);
		} else {
			return responseError(res, genResponseObj(req.get('x-language'), '50000', e.message, '', NODE));
		}
	}
};

exports.getRegistrationAllocatedSlotNumberListByCarSize = async (req, res) => {
	logger.root.debug('parking.controller get registration allocated slot number list by car size', req);
	try {
		const resultData = await service.getRegistrationAllocatedSlotNumberListByCarSize(req);
		return response(res, genResponseObj(req.get('x-language'), '20000', 'get registration allocated slot number list by car size success', resultData, NODE));
	} catch (e) {
		if (e && e.resultCode) {
			return responseError(res, e);
		} else {
			return responseError(res, genResponseObj(req.get('x-language'), '50000', e.message, '', NODE));
		}
	}
};

exports.getRegistrationPlateNumberListByCarSize = async (req, res) => {
	logger.root.debug('parking.controller get registration plate number list by car size', req);
	try {
		const resultData = await service.getRegistrationPlateNumberListByCarSize(req);
		return response(res, genResponseObj(req.get('x-language'), '20000', 'get registration plate number list by car size success', resultData, NODE));
	} catch (e) {
		if (e && e.resultCode) {
			return responseError(res, e);
		} else {
			return responseError(res, genResponseObj(req.get('x-language'), '50000', e.message, '', NODE));
		}
	}
};

exports.parkTheCar = async (req, res) => {
	logger.root.debug('parking.controller park the car', req);
	try {
		const transaction = await postgreSql.transaction();
		try {
			const { carNumberPlate } = req.body;
			const transactionHistory = await service.getTransactionHistoryByCriteria(req, {
				carNumberPlate,
				isRevoke: false
			}, [ { model: models['Parking'], where: { status: PARKING.STATUS.PENDING_ACTIVE }, required: true } ]);
			if (transactionHistory === null) {
				throw genResponseObj(req.get('x-language'), '40000', 'carNumberPlate is invalid', undefined, NODE);
			}
			await service.updateParkingByCriteria(req, { status: PARKING.STATUS.ACTIVE }, { parkingId: transactionHistory.parkingId }, transaction);
			await transaction.commit();
			return response(res, genResponseObj(req.get('x-language'), '20000', 'park the car success', '', NODE));
		} catch (e) {
			await transaction.rollback();
			if (e && e.resultCode) {
				throw e;
			} else {
				logger.root.error(e, req);
				throw genResponseObj(req.get('x-language'), '50000', e.message, '', NODE);
			}
		}
	} catch (e) {
		if (e && e.resultCode) {
			return responseError(res, e);
		} else {
			return responseError(res, genResponseObj(req.get('x-language'), '50000', e.message, '', NODE));
		}
	}
};

exports.leaveTheSlot = async (req, res) => {
	logger.root.debug('parking.controller leave the slot', req);
	try {
		const transaction = await postgreSql.transaction();
		try {
			const { carNumberPlate } = req.body;
			const transactionHistory = await service.getTransactionHistoryByCriteria(req, {
				carNumberPlate,
				isRevoke: false
			}, [ { model: models['Parking'], where: { status: PARKING.STATUS.ACTIVE }, required: true } ]);
			if (transactionHistory === null) {
				throw genResponseObj(req.get('x-language'), '40000', 'carNumberPlate is invalid', undefined, NODE);
			}
			await Promise.all([
				service.updateTransactionHistoryByCriteria(req, { isRevoke: true, logoutTime: new Date() },
					{ transactionHistoryId: transactionHistory.transactionHistoryId }, transaction),
				service.updateParkingByCriteria(req, { status: PARKING.STATUS.INACTIVE }, { parkingId: transactionHistory.parkingId }, transaction)
			]);

			if (ROLE.GUEST === req.get('x-auth-role')) {
				await service.updateRfidCardByCriteria(req, { status: RFID.STATUS.INACTIVE }, { rfidCardId: transactionHistory.rfidCardId }, transaction);
			}
			await transaction.commit();
			return response(res, genResponseObj(req.get('x-language'), '20000', 'leave the slot success', '', NODE));
		} catch (e) {
			await transaction.rollback();
			if (e && e.resultCode) {
				throw e;
			} else {
				logger.root.error(e, req);
				throw genResponseObj(req.get('x-language'), '50000', e.message, '', NODE);
			}
		}
	} catch (e) {
		if (e && e.resultCode) {
			return responseError(res, e);
		} else {
			return responseError(res, genResponseObj(req.get('x-language'), '50000', e.message, '', NODE));
		}
	}
};
