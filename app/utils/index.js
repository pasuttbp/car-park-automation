const { DATE_FORMAT } = require('../config/constants');
const moment = require('moment');
const camelcaseKeys = require('camelcase-keys');
const snakeCaseKeys = require('snakecase-keys');

const checkDateNowIsSameOrBefore = (date) => {
	return moment(date).isSameOrBefore(moment());
};

const generateSerial = () => {
	const dateString = moment().format('YYYYMMDDHHMMSSsss');
	const random = Math.round(Math.random() * 0xffff).toString(16);
	return `${ dateString }${ random }`;
};

const isEmptyObject = (obj) => {
	return !Object.keys(obj).length;
};

const convertTimestampUnixToDate = (timestamp) => (moment.unix(timestamp));

const convertTimestampToDate = (timestamp) => (moment(+timestamp));

const convertTimestampToDateUTC = (timestamp) => (moment.utc(+timestamp));

const convertSnakeCaseToCamelCase = (data) => (camelcaseKeys(data, { deep: true })); // object listObject

const convertCamelCaseToSnakeCase = (data) => (snakeCaseKeys(data, { deep: true })); // object listObject

const formatDate = (date, format) => {
	return moment(date).format(format);
};

const formatUTCDate = (date, format) => {
	return moment.utc(date).format(format);
};

const dateToTimestamp = (date = new Date()) => {
	return moment(date).valueOf();
};

const convertStringToDate = (date) => (moment(date, 'DD/MM/YYYY').toISOString());
const convertStringToUTCDate = (date) => (moment.utc(date, 'DD/MM/YYYY').toISOString());

const equalsIgnoreCase = (mainString, compareString) => {
	return mainString.toLowerCase() === compareString.toLowerCase();
};

const convertStringToNumber = (numberString) => {
	const number = new Number(numberString);
	if (isNaN(number)) {
		return null;
	} else {
		return number;
	}
};

isNaN(new Number('N/A'));

const getDateUTC = () => {
	const timestamp = moment(new Date()).valueOf();
	return moment.utc(+timestamp);
};


const getTimestamp = () => {
	return moment().format(DATE_FORMAT);
};

const convertJsonStringFormDataToJson = (data) => {
	return JSON.parse(data.replace(/'/g, '"'));
};

const isJson = (str) => {
	try {
		convertJsonStringFormDataToJson(str);
	} catch (e) {
		return false;
	}
	return true;
};
const stringToBoolean = (str) => (str === 'true');


module.exports = {
	dateToTimestamp,
	formatDate,
	formatUTCDate,
	convertStringToDate,
	convertTimestampToDate,
	convertTimestampToDateUTC,
	convertSnakeCaseToCamelCase,
	convertCamelCaseToSnakeCase,
	isEmptyObject,
	equalsIgnoreCase,
	convertStringToNumber,
	convertStringToUTCDate,
	getDateUTC,
	generateSerial,
	getTimestamp,
	convertJsonStringFormDataToJson,
	checkDateNowIsSameOrBefore,
	isJson,
	convertTimestampUnixToDate,
	stringToBoolean,

};
