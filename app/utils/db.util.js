const replaceSpecialCharacterInQuery = (sql = '') => {
  let textSql = sql;
  const specialCharacter = ['%', '_', '['];
  let tmp1 = '';
  if (specialCharacter.includes(textSql[0]) && textSql.length > 1) {
    tmp1 = textSql[0];
    textSql = textSql.substring(1);
  }
  let tmp2 = '';
  if (specialCharacter.includes(textSql[textSql.length - 1])) {
    tmp2 = textSql[textSql.length - 1];
    textSql = textSql.substring(0, textSql.length - 1);
  }
  let replaceText = textSql
    .split('')
    .map((c) => (specialCharacter.includes(c) ? `[${c}]` : c))
    .join('');
  return tmp1 + replaceText + tmp2;
};

module.exports = {
  replaceSpecialCharacterInQuery,
};
