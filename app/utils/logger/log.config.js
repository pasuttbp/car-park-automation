const moment = require('moment');
const winston = require('winston');
const {LOG} = require('../../config');
const {LOG_TYPE_SERVICE} = require('../../config/constants').LOG_CONSTANT;
const {format} = winston;
const {combine} = format;

require('winston-daily-rotate-file');

const LOG_TYPE = {
    ROOT: 'root',
    SUMMARY: 'summary',
    SERVICE: 'service'
};

const ROOT_LOG_PATH = `${LOG.ROOT_PATH}/${LOG_TYPE.ROOT}`;
const SUMMARY_LOG_PATH = `${LOG.ROOT_PATH}/${LOG_TYPE.SUMMARY}`;

// if new log add new LOG_TYPE_SERVICE and  new SERVICE_LOG_....._PATH
const SERVICE_LOG_MINI_EAI_PATH = `${LOG.ROOT_PATH}/${LOG_TYPE.SERVICE}/${LOG_TYPE_SERVICE.PARKING}`;

const ignorePrivate = format((info) => {
    if (info.private) {
        return false;
    }
    return info;
});

const getDailyRotateConfig = (
    filename,
    level = LOG.FILE_LEVEL,
    datePattern = 'YYYYMMDDHHmm',
) => {
    return {
        timestamp: () => {
            return moment();
        },
        filename,
        datePattern,
        level,
        handleExceptions: true,
        frequency: '15m',
        maxSize: '10m',
    };
};

const addLogger = (path, fileName) => {
    let lowerCaseFileName = fileName.toString().toLowerCase();
    let filePath = `${LOG.NAME}_${lowerCaseFileName}`;

    const logTransportCommand = [];
    logTransportCommand.push(
        new winston.transports.Console({
            level: LOG.CONSOLE_LEVEL,
            formatter: function (options) {
                return (undefined !== options.message ? options.message : '');
            },
            handleExceptions: true,
            json: false,
            colorize: true,
        })
    );

    if (path === LOG_TYPE.SERVICE) {
        path = `${path}/`;
    }

    const logFile = new winston.transports.DailyRotateFile(getDailyRotateConfig(`${path}/${filePath}_%DATE%.log`));
    logTransportCommand.push(logFile);

    winston.loggers.add(fileName, {
        format: combine(
            ignorePrivate(),
            // winston.format.align(),
            winston.format.printf((info) => `${info.message}`),
        ),
        transports: logTransportCommand,
    });
};

addLogger(ROOT_LOG_PATH, LOG_TYPE.ROOT);
addLogger(SUMMARY_LOG_PATH, LOG_TYPE.SUMMARY);
// if new service path new add log
addLogger(SERVICE_LOG_MINI_EAI_PATH, LOG_TYPE_SERVICE.PARKING);

module.exports = {
    LOG_TYPE
};
