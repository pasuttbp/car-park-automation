const moment = require('moment');
const winston = require('winston');
const {LOG_TYPE} = require('./log.config');
const {LOG_CONSTANT, DATE_FORMAT} = require('../../config/constants');

const logger = {
    root: {
        error: (message, req) => {
            message = typeof message === 'object' ? message.stack !== undefined ? message.stack : message : message;
            winston.loggers.get(LOG_TYPE.ROOT).error(formatRootLog(req, message, LOG_CONSTANT.LEVEL.ERROR));
        },
        info: (message, req) => {
            winston.loggers.get(LOG_TYPE.ROOT).info(formatRootLog(req, message, LOG_CONSTANT.LEVEL.INFO));
        },
        debug: (message, req) => {
            winston.loggers.get(LOG_TYPE.ROOT).debug(formatRootLog(req, message, LOG_CONSTANT.LEVEL.DEBUG));
        },
    },
    service: {
        info: (req, logServiceType, reqProperty, reqBody, status, body, reqTimestamp, resTimestamp) => {
            winston.loggers.get(logServiceType).info(modelServiceLog(req, reqProperty, reqBody, status, body, reqTimestamp, resTimestamp));
        },
        debug: (req, logServiceType, reqProperty, reqBody, status, body, reqTimestamp, resTimestamp) => {
            winston.loggers.get(logServiceType).debug(modelServiceLog(req, reqProperty, reqBody, status, body, reqTimestamp, resTimestamp));
        },
        error: (req, logServiceType, reqProperty, reqBody, status, body, reqTimestamp, resTimestamp) => {
            winston.loggers.get(logServiceType).error(modelServiceLog(req, reqProperty, reqBody, status, body, reqTimestamp, resTimestamp));
        }
    },
};

const getTimestamp = () => {
    return moment().format(DATE_FORMAT);
};

const modelServiceLog = (req, reqProperty, reqBody, status, body, reqTimestamp, resTimestamp) => {
    const usageTime = moment(resTimestamp, DATE_FORMAT).diff(moment(reqTimestamp, DATE_FORMAT), 'milliseconds');
    const logFormat = [
        getTimestamp(),
        req && req.headers['x-channel'] ? req.headers['x-channel'] || '' : '',
        req && req.headers['x-transaction-id'] ? req.headers['x-transaction-id'] || '' : '',
        // req && req.userInfo ? req.userInfo.userName : '',
        reqProperty && reqProperty.url ? reqProperty.url : '',
        reqProperty && reqProperty.method ? reqProperty.method : '',
        reqProperty && reqProperty.headers ? JSON.stringify(reqProperty.headers) : '',
        reqBody ? JSON.stringify(reqBody) : '',
        status ? status : '',
        body ? JSON.stringify(body) : '',
        usageTime
    ].join('|');

    return logFormat;
};


const formatRootLog = (req, message, level) => {
    const logFormat = [
        getTimestamp(),
        req && req.headers && req.headers['x-transaction-id'] ? req.headers['x-transaction-id'] || '' : '',
        // req && req.userInfo ? req.userInfo.userName : '',
        level,
        message && typeof message === 'object' && level !== LOG_CONSTANT.LEVEL.ERROR ? JSON.stringify(message) : message
    ].join('|');
    return logFormat;
};

/**
 * ************
 * Module.exports *
 * ************
 */

module.exports = {
    logger,
};
