# Node version

Node v.10.19.0

## Development server

Run `npm start` for a dev server. Application is started on `http://localhost:9090/`. The app will automatically reload if you change any of the source files.

## Install Docker and Docker Compose

Install Docker v20.10.7

## Create Docker image and save to file

Go to project workspace.

===== Build Docker image and save to file =====

ENV_APP = dev, sqa, iot, production

`docker build --no-cache -t thaivivat-test/car-park-automation --build-arg ENV_APP="dev" -f Dockerfile .`

`docker save -o path/dockerimage/car-park-automation.docker thaivivat-test/car-park-automation`

- docker load -i #image# 


## Start Program

1. Start Program with  docker-compose up -d 
2. Import Postman collection with "System Car Park.postman_collection.json"
3. Mockup data [ "Parking", "RfidCard", "Staff", "Role"] by GET  request to "http://localhost:9090/cpa/v1/mock"
4. API List test in postman
5. you can delete information database by config mock_up = N

### *Have Sequence Diagram
